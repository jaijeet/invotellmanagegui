export const environment = {
  production: true,
  Env:'dev',
  DomainUrl:'http://localhost:8080/invotell',
  ApiUrls:{
    getExistingIvr:'/v1/manage/ivr/single?ivrID={ivrId}',
    addNewIvr:'/v1/manage/ivr/add',
    deleteExistingIvr:'/v1/manage/ivr/deleteivrID={ivrId}',
    updateExistingIvr:'/v1/manage/ivr/update?ivrID={ivrId}',
    getCompleteIvrList:'/v1/manage/ivr/all',

    getIvrInfoByKeyword:'/v1/manage/ivrinfo/single?ivrID={ivrId}&keyword={keyword}',
    addNewIvrInfo:'/v1/manage/ivrinfo/add?ivrID={ivrId}',
    updateExistingIvrinfo:'/v1/manage/ivrinfo/update/single?ivrID={ivrId}&keyword={keyword}',
    deleteExistingIvrinfo:'/v1/manage/ivrinfo/delete?ivrID={ivrId}&keyword={keyword}',
    updateIvrInfoList:'/v1/manage/ivrinfo/update/multiple?ivrID={ivrId}',

    getQueuePbxTree:'/v1/manage/queue/ivrtree?ivrID={ivrId}',
    addVoicePrompt:'/v1/manage/queue/addprompt/{parentNode}/{status}?file={file}&ivrID={ivrId}',
    addNewQueue:'/v1/manage/queue/addqueue/{parentNode}?ivrID={ivrId}',

    addQueueInfoList:'/v1/manage/queueinfo/add/multiple/{ivrId}/{queueId}',
    
    ivrFileUpload:'/v1/manage/upload/ivr/{ivrId}/{keyword}',
    queuePromptFileUpload:'/v1/manage/upload/queue/{ivrId}/{queueId}',

    getSkillCategoryList:'/v1/manage/skill/category/list',
    getSkillList:'/v1/manage/skill/list/all',
    addSkillCategory:'/v1/manage/skill/category/add',
    addNewSkill:'/v1/manage/skill/add?catId={catId}',
    editSkillCategory:'/v1/manage/skill/category/update?skillCatId={catId}',
    editExistingSkill:'/v1/manage/skill/update?skillId={skillId}&catId={catId}',
    deleteSkillCategory:'/v1/manage/skill/category/delete?skillCatId={catId}&name={name}',
    deleteExistingSkill:'/v1/manage/skill/delete?skillId={skillId}',

    getCpuUsageChart:'/v1/dashboad/chart/cpu',
    getCpuMemoryUsageChart:'/v1/dashboad/chart/memory'
  }
};