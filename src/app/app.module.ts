import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './layout/header/header.component';
import { SideBarComponent } from './layout/side-bar/side-bar.component';
import { AppMaterialModule } from './common/app-material/app-material.module';
import { LoginComponent } from './common/login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HomeComponent } from './component/home/home.component';
import { UsersComponent } from './component/manage/users/users.component';
import { IvrComponent } from './component/manage/ivr/ivr.component';
import { DialogBoxComponent } from './common/diaglog-box/dialog-box.component';
import { ReactiveFormsModule, FormsModule  }  from '@angular/forms';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { QueuePromptComponent } from './component/manage/queue-prompt/queue-prompt.component';
import { VoiceMailComponent } from './component/manage/voice-mail/voice-mail.component';
import { ConferenceComponent } from './component/manage/conference/conference.component';
import { SocketClientComponent } from './component/test/socket-client/socket-client.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { LibraryComponent } from './component/manage/library/library.component';
import { ReportComponent } from './component/manage/report/report.component';
import { NgApexchartsModule } from "ng-apexcharts";
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideBarComponent,
    LoginComponent,
    DashboardComponent,
    HomeComponent,
    UsersComponent,
    IvrComponent,
    DialogBoxComponent,
    QueuePromptComponent,
    VoiceMailComponent,
    ConferenceComponent,
    SocketClientComponent,
    LibraryComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right'
    }),
    NgApexchartsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[DialogBoxComponent,QueuePromptComponent]
})
export class AppModule { }
