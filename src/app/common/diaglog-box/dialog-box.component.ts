import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { IvrService } from 'src/app/service/ivr.service';
import { DatePipe } from '@angular/common';
import { CommonConstant } from '../common-constant';
import { SkillService } from 'src/app/service/skill.service';


@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent implements OnInit {

  public dialogFormGroup: FormGroup;
  private pipe = new DatePipe('en-US');

  constructor(
    private dialogRef: MatDialogRef<DialogBoxComponent>,
    private formBuilder: FormBuilder,
    private _ivrService: IvrService,
    private _skillService: SkillService,
    @Inject(MAT_DIALOG_DATA) public data: DialogBoxModel.DialogData
  ) { }

  ngOnInit() {
    this.generateFormGroup();
  }

  private generateFormGroup() {
    this.dialogFormGroup = this.formBuilder.group({})
    this.data.formField.forEach(formElement => {
      const val= (formElement.value)?formElement.value:'';
      if (formElement.required) {
        this.dialogFormGroup.addControl(formElement.name,new FormControl( { value: formElement.value, disabled: !formElement.editable },Validators.required )); 
      } else {
        this.dialogFormGroup.addControl(formElement.name,new FormControl( { value: formElement.value, disabled: !formElement.editable })); 
      }
    });    
  }

  public onBtnClick(event, btn) {
    switch (btn.callBackValue) {

      case CommonConstant.DialogModelApi.Confirm:
        this.confirmBtnClicked(event, btn);
        break;

      case CommonConstant.DialogModelApi.CreateIvr:
        this.createIvrClicked();
        break;
      
      case CommonConstant.DialogModelApi.CreateSkillCategory:
        this.createSkillCategoryClicked();
        break;

      case CommonConstant.DialogModelApi.CreateNewSkill:
        this.createNewSkill();
        break;
        
      case CommonConstant.DialogModelApi.Cancel:
        this.dialogRef.close();
        break;
      
      case CommonConstant.DialogModelApi.EdiSkillCategory:
        this.editSkillCategory();
      break;

      case CommonConstant.DialogModelApi.EditExistingSkill:
        this.editExistingSkill();
      break;

      default:
        break;
    }    
  }
    
  private confirmBtnClicked(event: any, btn: any) {
    this.dialogRef.close('confirm');
  }

  private createIvrClicked() {
    if (this.dialogFormGroup.status == "VALID") {
      let data = this.getCreateIvrReqObj();
      this._ivrService.AddNewIvr(data).subscribe(resp => {
        if (resp) {
          this.dialogRef.close('created');
        }
      });
    }
  }

  private getCreateIvrReqObj() {
    let req = {};
    let ivr = {};
    Object.keys(this.dialogFormGroup.controls).forEach(controlName => {
      ivr[controlName] = this.dialogFormGroup.controls[controlName].value;
    });
    ivr['createdBy'] = 'jaijeet';
    const createDate = Date.now()
    ivr['createdOn'] = this.pipe.transform(createDate, 'yyyy-MM-dd HH:mm:ss');

    req['RequestInfo'] = ivr;
    return req;
  }

  private createSkillCategoryClicked(){
    if (this.dialogFormGroup.status == "VALID") {
      let data = this.getCreateSkillCategoryReqObj();
      this._skillService.AddSkillCategory(data).subscribe(resp=>{
        if(resp){
          this.dialogRef.close('created');
        }
      });
    }
  }

  private editSkillCategory() {
    if (this.dialogFormGroup.status == "VALID") {
      let data = this.getCreateSkillCategoryReqObj();
      const catId  =  this.dialogFormGroup.controls['skillCategoryID'].value;
      this._skillService.EditSkillCategory(data,catId).subscribe(resp=>{
        if(resp){
          this.dialogRef.close('updated');
        }
      });
    }
  }
  
  private getCreateSkillCategoryReqObj() {
    let req = {};
    let skillCategory = {};
    Object.keys(this.dialogFormGroup.controls).forEach(controlName => {
      skillCategory[controlName] = this.dialogFormGroup.controls[controlName].value;
    });
    req['RequestInfo'] = skillCategory;
    return req;
  }

  private createNewSkill() {
    if (this.dialogFormGroup.status == "VALID") {
      let data = this.getCreateSkillReqObj();
      const catid  =  this.dialogFormGroup.controls['catId'].value;
      this._skillService.AddNewSkill(data,catid).subscribe(resp=>{
        if(resp){
          this.dialogRef.close('created');
        }
      });
    }
  }

  private editExistingSkill() {
    if (this.dialogFormGroup.status == "VALID") {
      let data = this.getCreateSkillReqObj();
      const catId  =  this.dialogFormGroup.controls['catId'].value;
      const skillId  =  this.dialogFormGroup.controls['skillId'].value;
      this._skillService.EditExistingSkill(data,skillId,catId).subscribe(resp=>{
        if(resp){
          this.dialogRef.close('updated');
        }
      });
    }
  }

  private getCreateSkillReqObj() {
    let req = {};
    let skillObj = {};
    Object.keys(this.dialogFormGroup.controls).forEach(controlName => {
      if(controlName !='catId'){
        skillObj[controlName] = this.dialogFormGroup.controls[controlName].value;
      }
    });
    req['RequestInfo'] = skillObj;
    return req;
  }

}
