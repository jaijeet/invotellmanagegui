declare module DialogBoxModel{

    export interface DialogData{
        header:string;
        msg:string;
        formField:FormField[],
        btnMsg:string;
        btnlist:BtnList[];
    }
    export interface BtnList{
        name:string;
        isPrimary:boolean;
        apiCall:string;
    }
    export interface FormField{
        label:string,
        fieldType:string,
        name:string,
        options:any[],
        required:boolean,
        editable:boolean,
        value:any
    }
}