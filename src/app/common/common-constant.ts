export class CommonConstant {
    public static RouteKeys=[
        
    ]

    public static DialogModelApi={
        Cancel:'cancel',
        CreateIvr:'createIvr',
        Confirm:'confirm',
        CreateSkillCategory:'createSkillCategory',
        CreateNewSkill:'createNewSkill',
        EdiSkillCategory:'editSkillCategory',
        EditExistingSkill: 'editExistingSkill'
    }

    public static ManageBox={
        Url:'192.168.153.128',
        Mode:'api'
    }

}