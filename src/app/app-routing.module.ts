import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './common/login/login.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HomeComponent } from './component/home/home.component';
import { UsersComponent } from './component/manage/users/users.component';
import { IvrComponent } from './component/manage/ivr/ivr.component';
import { VoiceMailComponent } from './component/manage/voice-mail/voice-mail.component';
import { ConferenceComponent } from './component/manage/conference/conference.component';
import { LibraryComponent } from './component/manage/library/library.component';
import { ReportComponent } from './component/manage/report/report.component';


const routes: Routes = [
  {
    path:'',redirectTo:'/home/dashboard',pathMatch:'full',
  },
  {
    path:'home',redirectTo:'/home/dashboard',pathMatch:'full'
  },
  {
    path:'login',component:LoginComponent
  },
  {
    path:'home',component:HomeComponent,
    children:[
      {
        path:'dashboard',component:DashboardComponent
      },
      {
        path:'manage/users',component:UsersComponent
      },
      { 
        path:'manage/ivr',component:IvrComponent
      },
      {
        path:'manage/voicemail',component:VoiceMailComponent 
      },
      {
        path:'manage/conference',component:ConferenceComponent 
      },
      {
        path:'manage/library',component: LibraryComponent
      },
      {
        path:'manage/report',component: ReportComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
