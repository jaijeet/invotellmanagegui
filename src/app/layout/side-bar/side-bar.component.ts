import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  @Output()
  navUpdate = new EventEmitter();

  dashboardActive: boolean = true;
  step = -1;

  navigation = [
    {
      label: "Manage",
      route: "",
      icon: "important_devices",
      isActive: false,
      child: [
        {
          label: "System",
          route: "system",
          isActive: false
        },
        {
          label: "Users",
          route: "/home/manage/users",
          isActive: false
        },
        {
          label: "IVR",
          route: "/home/manage/ivr",
          isActive: false
        },
        {
          label: "Voicemail",
          route: "/home/manage/voicemail",
          isActive: false
        },
        {
          label: "Library",
          route: "/home/manage/library",
          isActive: false
        },
        {
          label: "Conference",
          route: "/home/manage/conference",
          isActive: false
        },
        {
          label: "Report",
          route: "/home/manage/report",
          isActive: false
        }            
      ]
    },
    {
      label: "Diagnostics",
      route: "diagnose",
      icon: "multiline_chart",
      isActive: false,
      child: [
        {
          label: "System",
          route: "system",
          isActive: false
        },
        {
          label: "Network",
          route: "system",
          isActive: false
        },
        {
          label: "Phone Lines",
          route: "system",
          isActive: false
        }
      ]
    }
  ];

  constructor(private router: Router) {
  }

  ngOnInit() {
    let url = this.router.url;
    if (url.indexOf('dashboard') !=-1) {
      this.updateNavigationLink('Dashboard', 'none',null);
    } else {
      console.log("inside the else");
      let navtitle = url.split("/");
      let nav:any;
      let subNav:any;

      this.navigation.forEach(element => {
          element.child.forEach(child => {
            if(child.route === url){
              nav = element;
              subNav= child;
            }
          });
      });

      this.updateNavigationLink(nav, subNav,null);
    }
  }

  updateNavigationLink(nav, subnav,$event) {
    if($event){
      console.log($event.target);
      $event.currentTarget.blur();
    }
    const data = this.updateActiveRoute(nav, subnav);
    this.navUpdate.emit(data);
  }
  setStep(index: number) {
    this.step = index;
  }

  updateActiveRoute(nav, subnav) {
    if (nav === 'Dashboard') {
      this.dashboardActive = true;
      const breadCrum: PageNavTile = {
        title: 'Dashboard', nav: [
          { title: 'Dashboard', route: '/home/dashboard' }
        ]
      };

      this.navigation.forEach(element => {
        element.isActive = false;
        element.child.forEach(child => {
          child.isActive = false;
        });
      });

      return breadCrum;
    } else {
      this.dashboardActive = false;
      let breadCrum: PageNavTile;
      this.navigation.forEach(element => {
        if (element.label === nav.label) {
          element.isActive = true;
          element.child.forEach(child => {
            if (child.label == subnav.label) {
              child.isActive = true;
              breadCrum = {
                title: subnav.label,
                nav: [
                  { title: element.label, route: element.route },
                  { title: subnav.label, route: subnav.route }
                ]
              };
            } else {
              child.isActive = false;
            }
          });
        } else {
          element.isActive = false;
          element.child.forEach(child => {
            child.isActive = false;
          });
        }
      });
      return breadCrum;
    }
  }

}

interface NavRoute {
  title: string,
  route: string
}

interface PageNavTile {
  title: string;
  nav: NavRoute[];
}
