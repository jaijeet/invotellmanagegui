import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexStroke, ApexTooltip, ApexDataLabels, ApexNonAxisChartSeries, ApexPlotOptions, ApexFill } from 'ng-apexcharts';
import { ToastrService } from 'ngx-toastr';
import { DashboardService } from 'src/app/service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // @ViewChild('callChartObj', { static: false }) callChart: ChartComponent;
  // @ViewChild('cpuChartObj', { static: false }) cpuChart: ChartComponent;


  public callChartOptions: Partial<AreaChartOptions> = {};
  public cpuChartOptions: Partial<AreaChartOptions> = {};
  public memoryChartOption: Partial<RadialChartOptions> = {};
  public swampChartOption: Partial<RadialChartOptions> = {};


  public cpuChartCreated: boolean = false;
  public ramCreated: boolean = false;
  public swapCreated: boolean = false;

  constructor(private _chartService: DashboardService, private toastr: ToastrService) {

  }

  ngOnInit() {
    this.createCallsGraph();
    this.createCpuUsageGraph();
    this.memoryCpuCheck();
  }

  private memoryCpuCheck() {
    this._chartService.GetCpuMemoryUsageChart().subscribe(resp => {
      let ram = resp['ResponseInfo']['ram'];
      let swap = resp['ResponseInfo']['swap'];
      this.createMemoryCircle(ram['total'], ram['used']);
      this.createSwampCircle(swap['total'], swap['used']);
    });
  }


  private createSwampCircle(total: number, usage: number) {
    let per = this.calculatePercentage(usage, total);
    console.log(per);
    this.swampChartOption = {
      series: [per],
      chart: {
        height: 350,
        type: "radialBar",
        offsetY: -10
      },
      plotOptions: {
        radialBar: {
          startAngle: -135,
          endAngle: 135,
          dataLabels: {
            name: {
              fontSize: "16px",
              color: undefined,
              offsetY: 120
            },
            value: {
              offsetY: 76,
              fontSize: "22px",
              color: undefined,
              formatter: function (val) {
                return ""+usage+" / "+total;;
              }
            }
          }
        }
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          shadeIntensity: 0.15,
          inverseColors: false,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 50, 65, 91]
        }
      },
      stroke: {
        dashArray: 4
      },
      labels: ["Swap (in MB)"]
    };
    this.swapCreated = true;
  }

  private createMemoryCircle(total: number, usage: number) {
    let per = this.calculatePercentage(usage, total);
    this.memoryChartOption = {
      series: [per],
      chart: {
        height: 350,
        type: "radialBar"
      },
      plotOptions: {
        radialBar: {
          startAngle: -135,
          endAngle: 225,
          hollow: {
            margin: 0,
            size: "70%",
            background: "#fff",
            image: undefined,
            position: "front",
            dropShadow: {
              enabled: true,
              top: 3,
              left: 0,
              blur: 4,
              opacity: 0.24
            }
          },
          track: {
            background: "#fff",
            strokeWidth: "50%",
            margin: 0, // margin is in pixels
            dropShadow: {
              enabled: true,
              top: -3,
              left: 0,
              blur: 4,
              opacity: 0.35
            }
          },

          dataLabels: {
            show: true,
            name: {
              offsetY: -10,
              show: true,
              color: "#888",
              fontSize: "17px"
            },
            value: {
              formatter: function (val) {
                return ""+usage+" / "+total;
              },
              color: "#111",
              fontSize: "36px",
              show: true
            }
          }
        }
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          type: "horizontal",
          shadeIntensity: 0.5,
          gradientToColors: ["#F54EA2"],
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100]
        }
      },
      stroke: {
        lineCap: "round"
      },
      labels: ["RAM Usage (in MB)"]
    };
    this.ramCreated = true;
  }

  private createCpuUsageGraph() {
    this._chartService.GetCpuUsageChart().subscribe(resp => {
      let data = [];
      let category = [];
      resp['ResponseInfo'].forEach(row => {
        data.push(row['usage']);
        category.push(row['time']);
      });


      this.cpuChartOptions.series = [
        {
          name: "CPU Usage",
          color: "#f45c89",
          data: data
        }
      ];
      this.cpuChartOptions.chart = { height: 350, type: "area" };
      this.cpuChartOptions.dataLabels = { enabled: false };
      this.cpuChartOptions.stroke = { curve: "smooth", colors: ["#f45c89"] };
      this.cpuChartOptions.xaxis = {
        type: "datetime",
        categories: category
      };
      this.cpuChartOptions.tooltip = { x: { format: "dd/MM/yy HH:mm" } };

      this.cpuChartCreated = true;
    });
  }

  private createCallsGraph() {
    this.callChartOptions.series = [
      {
        name: "series1",
        color: "#33c4e4",
        data: [31, 40, 28, 51, 42, 109, 100]
      },
      {
        name: "series2",
        color: "#8b82fa",
        data: [11, 32, 45, 32, 34, 52, 41]
      }
    ];
    this.callChartOptions.chart = { height: 350, type: "area" };
    this.callChartOptions.dataLabels = { enabled: false };
    this.callChartOptions.stroke = { curve: "smooth", colors: ["#33c4e4", "#8b82fa"] };
    this.callChartOptions.xaxis = {
      type: "datetime",
      categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z",
        "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
    };
    this.callChartOptions.tooltip = { x: { format: "dd/MM/yy HH:mm" } };
  }

  private calculatePercentage(actualValue: number, totalValue: number) {
    return (actualValue / totalValue) * 100;
  }

}

export type AreaChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
};

export type RadialChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  stroke: ApexStroke;
};