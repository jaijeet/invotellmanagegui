import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { CommonConstant } from 'src/app/common/common-constant';
import { QueueConstant } from 'src/app/component/manage/queue-prompt/QueueConstant';
import { FormGroup, FormControl } from '@angular/forms';
import timezones from 'timezones.json';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['userName', 'sipid', 'skill', 'extension','select'];
  dataSource = new MatTableDataSource<UserTableHeader>(USER_LIST_DATA);
  selection = new SelectionModel<UserTableHeader>(true, []);
  boxType=CommonConstant.ManageBox.Mode;
  enableCreateUserForm:boolean=false;
  userFormGroup:FormGroup;
  timeZoneDrop=timezones;

  public skillGroups = QueueConstant.skillGroup;
  constructor() { }

  ngOnInit() {
    console.log(this.dataSource);
    console.log(timezones);
    console.log(new Date());    
    this.createUserFormGroup();
  }

  createUserFormGroup() {
    this.userFormGroup = new FormGroup({});
    this.userFormGroup.addControl('firstName',new FormControl());
    this.userFormGroup.addControl('lastName',new FormControl());
    this.userFormGroup.addControl('email',new FormControl());
    this.userFormGroup.addControl('password',new FormControl());
    this.userFormGroup.addControl('extension',new FormControl());
    this.userFormGroup.addControl('phone',new FormControl());
    this.userFormGroup.addControl('timeZone',new FormControl());
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  createUser(isNew:boolean){
    if(isNew){

    }else{

    }
    this.enableCreateUserForm = true;
  }

  back(){
    this.enableCreateUserForm = false;
  }

}

export interface UserTableHeader {
  userName: string;
  sipid: string;
  skill: string[];
  extension:number;
}

const USER_LIST_DATA: UserTableHeader[] = [
  { userName:'demo',sipid:'1001',skill:['sdasd','poipoi'],extension:102},
  { userName:'demo',sipid:'1002',skill:['asdm,xcjch'],extension:103},
  { userName:'demo',sipid:'1003',skill:['asd,asds,xcxc'],extension:104},
  { userName:'demo',sipid:'1004',skill:null,extension:105}
]