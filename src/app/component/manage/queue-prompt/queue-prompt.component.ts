import { Component, OnInit, Inject } from '@angular/core';
import { QueueConstant } from './QueueConstant';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { QueueService } from 'src/app/service/queue.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-queue-prompt',
  templateUrl: './queue-prompt.component.html',
  styleUrls: ['./queue-prompt.component.css']
})
export class QueuePromptComponent implements OnInit {

  public queueFormGroup: FormGroup;
  public queueDataValid: boolean = false;

  public interimFormGroup: FormGroup;
  public interimDataValid: boolean = false;
  public promptFileName: string = '';

  public skillGroups = QueueConstant.skillGroup;

  selectedNodeType: string = "interim";
  showNodeSelectionPage: boolean = true;
  showQueueAddPage: boolean = false;
  showPromptAddPage: boolean = false;
  keyList = QueueConstant.keyPressList;
  promptStatus: string = "enable";

  private ivrID;
  private queueID;
  private parentNode;
  private promptFile: any = null;

  constructor(private dialogRef: MatDialogRef<QueuePromptComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogBoxModel.DialogData, private _queueService: QueueService, private toastr: ToastrService) { }

  ngOnInit() {
    console.log(this.data);
    // {action: "add", ivrID: 110, nodeId: "0", qid: 501, keypress: Array(0)}
    this.ivrID = this.data['ivrID'];
    this.queueID = this.data['qid'];
    this.parentNode = this.data['nodeId'];
    this.interimFormGroup = new FormGroup({});
    this.queueFormGroup = new FormGroup({});
    if (this.data['keypress']) {
      this.data['keypress'].forEach(i => {
        this.keyList = this.keyList.filter(obj => obj.value !== i);
      });
    }
  }

  public selectNodeType() {
    if (this.selectedNodeType == "interim") {
      this.createInterimFormGroup();
      this.showNodeSelectionPage = false;
      this.showQueueAddPage = false;
      this.showPromptAddPage = true;
    } else {
      this.createQueueFormGroup();
      this.showNodeSelectionPage = false;
      this.showQueueAddPage = true;
      this.showPromptAddPage = false;
    }
  }
  private createQueueFormGroup() {
    this.queueFormGroup.addControl('queueName', new FormControl('', Validators.required));
    this.queueFormGroup.addControl('keyPress', new FormControl('', Validators.required));
    this.queueFormGroup.addControl('ringMode', new FormControl('all', Validators.required));
    this.queueFormGroup.addControl('predicAnnounceFrequency', new FormControl('30', Validators.required));
    this.queueFormGroup.addControl('autoConnectCall', new FormControl('yes', Validators.required));
    this.queueFormGroup.addControl('ringNextAgentAfter', new FormControl('05', Validators.required));
    this.queueFormGroup.addControl('onAgentReject', new FormControl('NextAgent', Validators.required));
    this.queueFormGroup.addControl('forwardCall', new FormControl('voicemail', Validators.required));
  }

  private createInterimFormGroup() {
    this.interimFormGroup.addControl('queueName', new FormControl('', Validators.required));
    this.interimFormGroup.addControl('promptStatus', new FormControl('', Validators.required));
    this.interimFormGroup.addControl('keyPress', new FormControl('', Validators.required));
    this.interimFormGroup.addControl('promptFile', new FormControl());
  }

  public backToSelection() {
    this.showNodeSelectionPage = true;
    this.showQueueAddPage = false;
    this.showPromptAddPage = false;
  }

  public closeDiv() {
    this.dialogRef.close();
  }

  public uploadQueuePromptFile() {
    this._queueService.UploadQueuePromptFile(this.data['ivrID'], this.data['qid'], this.promptFile).subscribe(resp => {
      if (resp['ResponseInfo'] && resp['ResponseInfo']['code'] == 200) {
        this.toastr.success(resp['ResponseInfo']['message']);
        this.promptFileName = this.promptFile.name;
        this.interimFormGroup.controls['promptFile'].setValue('');
      }
    }, error => {
      let resp = error.error['ResponseInfo'];
      resp.forEach(ex => {
        this.toastr.error(ex['Message']);
      });
    });
  }
  public handleFileInput(fileList: FileList, keyword) {
    if (keyword == "promptFile") {
      this.promptFile = fileList[0];
    }
  }

  public saveVoicePrompt() {
    Object.keys(this.interimFormGroup.controls).forEach(control => {
      console.log(control, this.interimFormGroup.controls[control]);
    });
    if (this.interimFormGroup.status == "INVALID") {
      Object.keys(this.interimFormGroup.controls).forEach(control => {
        this.interimFormGroup.controls[control].markAsTouched();
      });
    } else if (this.promptFileName === '') {
      this.toastr.error("Upload the prompt file.");
    } else {
      const data = {
        RequestInfo: {
          onKey: this.interimFormGroup.controls['keyPress'].value,
          queueID: this.queueID,
          queueName: this.interimFormGroup.controls['queueName'].value
        }
      }
      const status = (this.interimFormGroup.controls['promptStatus'].value == "") ? 'true' : 'false';
      this._queueService.AddNewVoicePrompt(this.ivrID, this.queueID, this.parentNode, status, data, this.promptFileName).subscribe(resp => {
        this.dialogRef.close('created');
      }, error => {
        this.toastr.error(error);
      });
    }
  }

  public saveQueueDetails() {
    if (this.queueFormGroup.status != "VALID") {
      this.toastr.error("Enter all the required fields");
      Object.keys(this.queueFormGroup.controls).forEach(controlName => {
        if (this.queueFormGroup.controls[controlName].status != "VALID") {
          this.queueFormGroup.controls[controlName].markAsTouched();
        }
      });
    } else {
      // in future adding the skill and skill detail page

      const queueReqObj = {
        RequestInfo: {
          onKey: this.queueFormGroup.controls['keyPress'].value,
          queueID: this.queueID,
          queueName: this.queueFormGroup.controls['queueName'].value
        }
      };

      let queueInfoObj = [];
      Object.keys(this.queueFormGroup.controls).forEach(controlName => {
        const info = { keyword: controlName, qid: this.queueID, value: this.queueFormGroup.controls[controlName].value };
        queueInfoObj.push(info);
      });


      // SAVE THE QUEUE
      this._queueService.AddNewQueue(this.ivrID, this.parentNode, queueReqObj).subscribe(resp => {
        if (resp['ResponseInfo']['code'] == 201) {
          this._queueService.AddQueueInfoList(this.ivrID, this.queueID, { RequestInfo: queueInfoObj }).subscribe(resp => {
            this.dialogRef.close('created');
          }, error => {
            this.toastr.error(error);
          });
        }else{
          this.toastr.error('Erorr is coming');
        }
      }, error => {
        this.toastr.error(error);
      });
    }
  }
}
