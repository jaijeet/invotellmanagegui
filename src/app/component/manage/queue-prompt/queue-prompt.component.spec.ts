import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuePromptComponent } from './queue-prompt.component';

describe('QueuePromptComponent', () => {
  let component: QueuePromptComponent;
  let fixture: ComponentFixture<QueuePromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueuePromptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuePromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
