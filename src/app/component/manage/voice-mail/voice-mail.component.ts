import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-voice-mail',
  templateUrl: './voice-mail.component.html',
  styleUrls: ['./voice-mail.component.css']
})
export class VoiceMailComponent implements OnInit {
  
  displayedColumns: string[] = [ 'name','size','createdOn','phoneNumber','queue','action'];
  dataSource = new MatTableDataSource<VoicemailHeader>(VOICEMAIL_LIST_DATA);
  constructor() { }

  ngOnInit() {
  }
}

export interface VoicemailHeader {
  name:string,
  size:string,
  createdOn: number;
  phoneNumber: number;
  queue: string;
}

const VOICEMAIL_LIST_DATA:VoicemailHeader[]=[
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'11062KB',createdOn:987465465,phoneNumber:8700396385,queue:'Humara'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'21062KB',createdOn:987465465,phoneNumber:8700396385,queue:'Humara'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'31062KB',createdOn:5987465465,phoneNumber:8700396385,queue:'VOICEMAIL'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'41062KB',createdOn:987465465,phoneNumber:8700396385,queue:'Humara'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'51062KB',createdOn:987465465,phoneNumber:8700396385,queue:'VOICEMAIL'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'51062KB',createdOn:987465465,phoneNumber:8700396385,queue:'Humara'},
  {name:'vm_2020-08-01_10:00:44_8700396385_default_VOICEMAIL.mp3',size:'61062KB',createdOn:987465465,phoneNumber:8700396385,queue:'VOICEMAIL'}
]
