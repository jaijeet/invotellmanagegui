import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from 'src/app/common/diaglog-box/dialog-box.component';
import { IvrConstant } from './ivrConstant';
import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { QueuePromptComponent } from '../queue-prompt/queue-prompt.component';
import { IvrService } from 'src/app/service/ivr.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { parse } from 'date-fns';

@Component({
  selector: 'app-ivr',
  templateUrl: './ivr.component.html',
  styleUrls: ['./ivr.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('200ms ease-in', style({ transform: 'translateX(0%' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateX(100%' }))
      ])
    ])
  ]
})
export class IvrComponent implements OnInit {

  public ivrListAvailable: boolean = false;
  public showIvrSettingPage: boolean = false;
  public ivrFormGroup: FormGroup;
  public ivrCompleteData: any;
  public ivrInfoData: any;
  public ivrStatus: string = "";
  public weekList: any;
  public destination: any;
  public offDays: DatePicker[];
  public disableRadios:boolean=false;

  displayedColumns: string[] = ['createdOn', 'ivrName', 'ivrID', 'setting', 'delete'];
  dataSource: MatTableDataSource<any>;

  private holidayCount: number = 1;
  private welcomeFile: any = null;
  private musicOnHold: any = null;
  private nightModeFile: any = null;
  private pipe = new DatePipe('en-US');
  private pbxTree:any;
  private nextQueueId:number;
  private nextIvr:number;

  //For the Queue Table
  displayedQueueColumns: string[] = ['keyName', 'type', 'view', 'action'];
  queueDataSource: MatTableDataSource<any>=new MatTableDataSource(); // = new MatTableDataSource<QueueTableHeader>(QUEUE_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private dialog: MatDialog, private _formBuilder: FormBuilder, private _ivrService: IvrService, private toastr: ToastrService) { }

  ngOnInit() {
    this.weekList = IvrConstant.WeekDays;
    this.destination = IvrConstant.DestinationType;
    this.createIvrListTable();
    this.createIvrFormGroup(null);
    this.nextQueueId=500;
    this.nextIvr=110;
  }

  public createIvrPopUp() {
    let ivrConstantModel = IvrConstant.CreateIvrDialogData;
    ivrConstantModel.formField[0].value=this.nextIvr;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: ivrConstantModel
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'created') {
        this.createIvrListTable();
      }
    });
  }

  public deleteIvrPopup(ivr) {

    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '450px',
      data: IvrConstant.DeleteIvrDialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
        this._ivrService.DeleteExistingIvr(ivr).subscribe(resp => {
          this.toastr.success(resp['ResponseInfo']);
          this.createIvrListTable();
        });
      }
    });
  }

  public ivrSetting(ivrId) {
    this._ivrService.GetExistingIvr(ivrId).subscribe((resp: any) => {
      this.ivrCompleteData = resp['ResponseInfo'];
      this.createIvrFormGroup(resp['ResponseInfo']);
      this.showIvrSettingPage = true;
    });
    this.updateQueueInformation(ivrId);
  }
  private updateQueueInformation(ivrId) {
    this._ivrService.GetQueuePbxTreeByIvrId(ivrId).subscribe((resp: any) => {
      this.queueDataSource = new MatTableDataSource(resp['ResponseInfo']);
      this.pbxTree = resp['ResponseInfo'];
      this.pbxTree.forEach(row => {
          if(this.nextQueueId<row.qid){
            this.nextQueueId = row.qid;
          }
      });
      this.nextQueueId++;
    });
  }

  public saveOfficeHours(ivrId) {
    console.log(this.ivrFormGroup);
    let errorExist = this.validatedOfficeHours();
    if (!errorExist) {
      const reqObj = {RequestInfo: this.createOfficeHourObj()};
      console.log(reqObj);
      this._ivrService.UpdateMultipleIvrInfo(ivrId,reqObj).subscribe(resp=>{
        console.log(resp);
      });
    }
  }

  private createOfficeHourObj() {
    let reqObj = [];
    var destinationMatch = this.ivrFormGroup.controls['destinationMatch'].value;
    var destinationNotMatch = this.ivrFormGroup.controls['destinationNotMatch'].value;
    var startTime = this.ivrFormGroup.controls['startTime'].value;
    var endTime = this.ivrFormGroup.controls['endTime'].value;
    var startDay = this.ivrFormGroup.controls['startDay'].value;
    var endDay = this.ivrFormGroup.controls['endDay'].value;

    reqObj.push({ keyword: "startTime", value: startTime });
    reqObj.push({ keyword: "endTime", value: endTime });
    reqObj.push({ keyword: "startDay", value: startDay });
    reqObj.push({ keyword: "endDay", value: endDay });
    reqObj.push({ keyword: "destinationMatch", value: destinationMatch });
    reqObj.push({ keyword: "destinationNotMatch", value: destinationNotMatch });

    Object.keys(this.ivrFormGroup.controls).forEach(control => {
      if (control.indexOf("holiday") != -1) {
        let val = this.ivrFormGroup.controls[control].value;
        const holi = this.pipe.transform(new Date(val), 'MM/dd/yyyy');
        reqObj.push({ keyword: control, value: holi });
      }
    });
    return reqObj;
  }

  private validatedOfficeHours() {
    let errorExist: boolean = false;
    let holidayError: boolean = false;
    Object.keys(this.ivrFormGroup.controls).forEach(control => {
      if (control.indexOf("holiday") != -1) {
        if (this.ivrFormGroup.controls[control].value == "") {
          this.addRequiredError(control);
          holidayError = true;
        }
      }
    });
    if (holidayError) {
      errorExist = true;
      this.toastr.error("Please select the holiday date");
    }
    if (this.validateDestinationMatch()) {
      errorExist = true;
    }
    if (this.validateStartEndDay()) {
      errorExist = true;
    }
    if (this.validateStartEndTime()) {
      errorExist = true;
    }
    return errorExist;
  }

  private validateDestinationMatch(): boolean {
    let errorExist: boolean = false;
    var destinationMatch = this.ivrFormGroup.controls['destinationMatch'].value;
    var destinationNotMatch = this.ivrFormGroup.controls['destinationNotMatch'].value;
    if ((destinationMatch == 0 || destinationMatch == "") || (destinationNotMatch == 0 || destinationNotMatch == "")) {
      errorExist = true;
      if ((destinationMatch == 0 || destinationMatch == "") && (destinationNotMatch == 0 || destinationNotMatch == "")) {
        this.addRequiredError('destinationMatch');
        this.addRequiredError('destinationNotMatch');
      } else if ((destinationMatch == 0 || destinationMatch == "")) {
        this.addRequiredError('destinationMatch');
      } else {
        this.addRequiredError('destinationNotMatch');
      }
      this.toastr.error("Please destination when it will match office hours or not");
    }
    return errorExist;
  }

  private validateStartEndTime(): boolean {
    let errorExist: boolean = false;
    var startTime = this.ivrFormGroup.controls['startTime'].value;
    var endTime = this.ivrFormGroup.controls['endTime'].value;
    if (startTime.length == 0 || endTime.length == 0) {
      errorExist = true;
      if (startTime.length == 0 && endTime.length == 0) {
        this.addRequiredError('startTime');
        this.addRequiredError('endTime');
      } else if (startTime.length == 0) {
        this.addRequiredError('startTime');
      } else {
        this.addRequiredError('endTime');
      }
      this.toastr.error("Please select office start time and end time");
    }
    return errorExist;
  }

  private validateStartEndDay(): boolean {
    let errorExist: boolean = false;
    var startDay = this.ivrFormGroup.controls['startDay'].value;
    var endDay = this.ivrFormGroup.controls['endDay'].value;

    if ((startDay.length == 0 || startDay == "") || (endDay.length == 0 || endDay == "")) {
      errorExist = true;
      if ((startDay.length == 0 || startDay == "") && (endDay.length == 0 || endDay == "")) {
        this.addRequiredError('startDay');
        this.addRequiredError('endDay');
      } else if ((startDay.length == 0 || startDay == "")) {
        this.addRequiredError('startDay');
      } else {
        this.addRequiredError('endDay');
      }
      this.toastr.error("Please select office start day and end day");
    }
    return errorExist;
  }

  private addRequiredError(controlName) {
    this.ivrFormGroup.controls[controlName].setErrors(Validators.required);
    this.ivrFormGroup.controls[controlName].markAsTouched();
  }

  private createIvrListTable() {
    this._ivrService.GetCompleteIvrList().subscribe((resp: any) => {
      this.dataSource = new MatTableDataSource(resp['ResponseInfo']);
      if(resp['ResponseInfo'].length > 0){
        this.ivrListAvailable=true;  
        resp['ResponseInfo'].forEach(ivr => {
          this.nextIvr = (ivr['ivrID']+1);
        });
      }else{
        this.ivrListAvailable=false;
        this.nextIvr=110;
      }
    });
  }

  private createIvrFormGroup(data) {
    this.ivrFormGroup = new FormGroup({});
    if (data != null) {
      this.ivrInfoData = {};
      data['ivrInfo'].forEach(element => {
        this.ivrInfoData[element['keyword']] = element['value'];
        this.ivrFormGroup.addControl(element['keyword'], new FormControl());
        if (element['keyword'] != 'inbound' && element['keyword'] != 'welcomeFile' && element['keyword'] != 'onHoldMusic' && element['keyword'] != 'nightmodeWelcome') {
          this.ivrFormGroup.controls[element['keyword']].setValue(element['value']);
        }
        if(element['keyword']=="nightmodeDestination"){
          if(element['value']=="voicemail"){
              this.disableRadios=true;
          }
        }

        if (element['keyword'].indexOf("holiday") != -1) {
          const starting = parseInt(element['keyword'].substring(7));
          this.holidayCount = (starting>this.holidayCount)?starting:this.holidayCount;
            if(!this.offDays){
              this.offDays=[];
            }
            const index = this.offDays.length + 1;
            const arr: DatePicker = { name: 'holiday' + starting, count: index, picker: 'picker' + starting };
            this.offDays.push(arr);
            const date =  parse(element['value'], 'MM/dd/yyyy', new Date());
            this.ivrFormGroup.controls[element['keyword']].setValue(date);
            this.holidayCount++;
        }
      });
    }

  }

  public backtoList() {
    this.showIvrSettingPage = false;
  }

  public addNewField() {
    if (this.offDays) {
      const index = this.offDays.length + 1;
      const arr: DatePicker = { name: 'holiday' + this.holidayCount, count: index, picker: 'picker' + this.holidayCount };
      this.ivrFormGroup.addControl('holiday' + this.holidayCount, new FormControl(''));
      this.offDays.push(arr);
      this.holidayCount++;
    } else {
      this.offDays = [];
      const arr: DatePicker = { name: 'holiday1', count: 1, picker: 'picker1' };
      this.offDays.push(arr);
      this.ivrFormGroup.addControl('holiday1', new FormControl(''));
      this.holidayCount++;
    }
  }

  public removeExistingField(event, offDay) {
    this.ivrFormGroup.removeControl(offDay.name);
    this.offDays = this.offDays.filter((element) => {
      if (element.name != offDay.name) {
        return true;
      } else {
        return false;
      }
    });
    console.log(this.offDays);
  }

  public onClickIvrSetup(action, ivr, node) {
    switch (action) {
      case 'add':
        this.createQueueOrPrompt(action,ivr, node);
        break;

    }
  }

  private createQueueOrPrompt(action,ivr, node) {
    let List = this.pbxTree.filter(item => item.parentNode == (node));
    let keypress = [];
    List.forEach(element => {
        keypress.push(element.keyPress);
    });
    const createData = {action:action,ivrID:ivr,nodeId:node,qid:this.nextQueueId,keypress:keypress};
    const dialogRef = this.dialog.open(QueuePromptComponent, {
      disableClose: true,
      width: '700px',
      data: createData
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result=='created'){
        this.updateQueueInformation(ivr);
      }
    });
  }

  public addInboundNumber(ivrId) {
    if (this.ivrFormGroup.controls['inbound'].value == "" || this.ivrFormGroup.controls['inbound'].value == null) {
      this.toastr.error("Invalid inbound number");
    } else {
      const reqObj = { RequestInfo: { keyword: "inbound", value: this.ivrFormGroup.controls['inbound'].value, ivr_id: ivrId } };
      this._ivrService.UpdateExistingIvrInfo(ivrId, 'inbound', reqObj).subscribe(resp => {
        this.ivrInfoData['inbound'] = this.ivrFormGroup.controls['inbound'].value;
        this.ivrFormGroup.controls['inbound'].setValue('');
      });
    }
  }

  public removeInboundNumber(ivrId) {
    const reqObj = { RequestInfo: { keyword: "inbound", value: '', ivr_id: ivrId } };
    this._ivrService.UpdateExistingIvrInfo(ivrId, 'inbound', reqObj).subscribe(resp => {
      this.ivrInfoData['inbound'] = '';
      this.ivrFormGroup.controls['inbound'].setValue('');
    });
  }

  public uploadIvrFile(ivrId, keyword) {
    let file: File = null;
    if (keyword == 'welcomeFile') {
      file = this.welcomeFile;
    } else if (keyword == 'onHoldMusic') {
      file = this.musicOnHold;
    } else {
      file = this.nightModeFile;
    }
    this._ivrService.UploadIvrFile(ivrId, keyword, file).subscribe(resp => {
      if (resp['ResponseInfo'] && resp['ResponseInfo']['code'] == 200) {
        this.toastr.success(resp['ResponseInfo']['message']);
        this.ivrInfoData[keyword] = file.name;
        this.ivrFormGroup.controls[keyword].setValue('');
      }
    }, error => {
      let resp = error.error['ResponseInfo'];
      resp.forEach(ex => {
        this.toastr.error(ex['Message']);
      });
    });
  }

  public handleFileInput(fileList: FileList, keyword) {
    if (keyword == 'welcomeFile') {
      this.welcomeFile = fileList[0];
    } else if (keyword == 'onHoldMusic') {
      this.musicOnHold = fileList[0];
    } else {
      this.nightModeFile = fileList[0];
    }
  }

  public restrictNumberOnly(event: any) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  public nightModeRadio(event,keyword){
    console.log(event,keyword,event.value);
    if(event.value=="forward"){
      this.disableRadios = false;
    }else{
      this.disableRadios = true;
    }
    const data = {RequestInfo:{ keyword: keyword, value: event.value }};
    this._ivrService.UpdateExistingIvrInfo(this.ivrCompleteData.ivrID,keyword,data).subscribe(resp=>{
      console.log(resp);
    },error=>{
      console.log(error);
    });
  }

}

export interface IvrTableHeader {
  createdOn: number;
  ivrName: string;
  ivrId: number;
}

export interface DatePicker {
  name: string,
  count: number,
  picker: string
}

const IVR_LIST_DATA: IvrTableHeader[] = [
  { createdOn: 1567969066193, ivrName: 'Demo', ivrId: 101 },
  { createdOn: 1567759166193, ivrName: 'Test', ivrId: 102 },
  { createdOn: 1567259086193, ivrName: 'Humara', ivrId: 103 },
  { createdOn: 1567019066193, ivrName: 'Demo', ivrId: 101 },
  { createdOn: 1565009166193, ivrName: 'Test', ivrId: 102 },
  { createdOn: 1564509086193, ivrName: 'Humara', ivrId: 103 }
]


export interface QueueTableHeader {
  keyName: string;
  type: string;
  padding: number;
}

const QUEUE_DATA: QueueTableHeader[] = [
  { keyName: "1-First", type: 'Queue', padding: 1 },
  { keyName: "2-Gap", type: 'Voice Prompt', padding: 1 },
  { keyName: " 1-INKNK", type: 'Queue', padding: 2 },
  { keyName: "3-Second", type: 'Queue', padding: 1 },
  { keyName: "4-Third", type: 'Queue', padding: 1 }
]

const QUEUE_DATA_SECOND: QueueTableHeader[] = [];