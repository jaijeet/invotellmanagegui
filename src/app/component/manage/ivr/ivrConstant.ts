import { CommonConstant } from 'src/app/common/common-constant'

export class IvrConstant {
    public static CreateIvrDialogData = {
        header: 'Add IVR',
        msg: '',
        formField: [
            {
                label: 'IVR ID:',
                fieldType: 'input',
                name: 'ivrID',
                options: null,
                required: true,
                editable:false,
                value:null
            },
            {
                label: 'IVR Name:',
                fieldType: 'input',
                name: 'ivrName',
                options: null,
                required: true,
                editable:true,
                value:null
            }
        ],
        btnMsg: "You will need to setup the IVR via the 'Settings' option.",
        btnlist: [
            {
                name: 'Create',
                isPrimary: true,
                callBackValue: CommonConstant.DialogModelApi.CreateIvr
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: CommonConstant.DialogModelApi.Cancel
            }
        ]
    }

    public static DeleteIvrDialogData = {
        header: 'Delete IVR',
        msg: '',
        formField: [],
        btnMsg: "Are you sure you want to delete?",
        btnlist: [
            {
                name: 'Ok',
                isPrimary: true,
                callBackValue: CommonConstant.DialogModelApi.Confirm
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: CommonConstant.DialogModelApi.Cancel
            }
        ]
    }

    public static WeekDays=[
        {
            value:1,
            name:"Sunday"
        },
        {
            value:2,
            name:"Monday"
        },
        {
            value:3,
            name:"Tuesday"
        },
        {
            value:4,
            name:"Wednesday"
        },
        {
            value:5,
            name:"Thursday"
        },
        {
            value:6,
            name:"Friday"
        },
        {
            value:7,
            name:"Saturday"
        }
    ]

    public static DestinationType=[
        {
            value:'ivr',
            name:'IVR'
        },
        {
            value:'voicemail',
            name:'Voice Mail'
        },
        {
            value:'terminate',
            name:'Terminate Call'
        },
        {
            value:'night',
            name:'Night-Mode'
        }
    ]
}