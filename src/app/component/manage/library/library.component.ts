import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { SkillService } from 'src/app/service/skill.service';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { LibraryConstant } from './library.constant';
import { DialogBoxComponent } from 'src/app/common/diaglog-box/dialog-box.component';
import { CommonConstant } from 'src/app/common/common-constant';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  public skillCategoryTable: MatTableDataSource<any>;
  public skillTable: MatTableDataSource<any>;
  public skillCategoryList: any[];
  public skillList: any[];

  public skillSearchFormGroup: FormGroup;

  displayedSkillCategoryColumns: string[] = ['name', 'setting', 'delete'];
  displayedSkillColumnn: string[] = ['name', 'skillcategory', 'setting', 'delete'];
  categoryDrop: string = "all";

  private subscription: Subscription = new Subscription();
  private nextSkillCatId: number = 1;
  private nextSkill: number = 1;

  constructor(private dialog: MatDialog, private _formBuilder: FormBuilder, private _skillService: SkillService, private toastr: ToastrService) { }


  ngOnInit() {
    this.getSkillCategoryList();
    this.buildSearchForm();
  }

  private buildSearchForm() {
    this.skillSearchFormGroup = new FormGroup({});
    this.skillSearchFormGroup.addControl('catDropDown', new FormControl('all'));
    this.skillSearchFormGroup.addControl('catSearchInput', new FormControl());

  }

  tabChanged(event: MatTabChangeEvent) {
    if (event.index == 0) {
      this.getSkillCategoryList();
    } else {
      this.getSkillList();
    }
  }

  private getSkillCategoryList() {
    const sub = this._skillService.GetSkillCategoryList().subscribe((resp: any) => {
      this.skillCategoryList = resp.ResponseInfo;
      this.skillCategoryTable = new MatTableDataSource(this.skillCategoryList);
      if (this.skillCategoryList.length > 0) {
        this.skillCategoryList.forEach(skillCategory => {
          this.nextSkillCatId = skillCategory['skillCategoryID'] + 1;
        })
      }
    });
    this.subscription.add(sub);
  }

  private getSkillList() {
    const sub = this._skillService.GetSkillList().subscribe((resp: any) => {
      this.skillList = resp.ResponseInfo;
      this.skillTable = new MatTableDataSource(this.skillList);
      if (this.skillList.length > 0) {
        this.skillList.forEach(skill => {
          this.nextSkill = skill['skillId'] + 1;
        })
      }
    });
    this.subscription.add(sub);
  }

  OnSearchSkill() {
    let selectedSkillcategory = this.skillSearchFormGroup.controls['catDropDown'].value;
    let searchText = (this.skillSearchFormGroup.controls['catSearchInput'].value) ? this.skillSearchFormGroup.controls['catSearchInput'].value : '';

    const filteredArray = this.skillList.filter(skill => {
      if (selectedSkillcategory == 'all') {
        if (searchText == '') {
          return true;
        } else {
          return (skill['name'].toLowerCase().indexOf(searchText.toLowerCase()) > -1) ? true : false;
        }
      } else {
        if (searchText == '' && selectedSkillcategory == skill['skillCategoryId']) {
          return true;
        } else if (selectedSkillcategory == skill['skillCategoryId']) {
          return (skill['name'].toLowerCase().indexOf(searchText.toLowerCase()) > -1) ? true : false;
        } else {
          return false;
        }
      }
    });
    this.skillTable = new MatTableDataSource(filteredArray);
    console.log(this.skillTable);
  }

  public createSkillCategory() {
    let skillCategoryConstant = JSON.parse(JSON.stringify(LibraryConstant.CreateSkillCategoryDialogData));
    skillCategoryConstant.formField[0].value = this.nextSkillCatId;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: skillCategoryConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'created') {
        this.getSkillCategoryList();
      }
    });
  }

  public addNewSkill() {
    let skillConstant = JSON.parse(JSON.stringify(LibraryConstant.CreateSkillDialogData));
    skillConstant.formField[0].value = this.nextSkill;

    let catOption = [];
    this.skillCategoryList.forEach(category => {
      const option = { key: category['name'], value: category['skillCategoryID'] };
      catOption.push(option);
    });
    skillConstant.formField[1].options = catOption;

    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: skillConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'created') {
        this.getSkillList();
        this.skillSearchFormGroup.controls['catDropDown'].setValue('all');
      }
    });

  }

  public editSkillCategory(element){
    let skillCategoryConstant = JSON.parse(JSON.stringify(LibraryConstant.CreateSkillCategoryDialogData));
    skillCategoryConstant.header='Edit Skill Category';
    skillCategoryConstant.formField[0].value = element['skillCategoryID'];
    skillCategoryConstant.formField[1].value = element['name'];
    skillCategoryConstant.btnlist[0].name = 'Edit';
    skillCategoryConstant.btnlist[0].callBackValue = CommonConstant.DialogModelApi.EdiSkillCategory;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: skillCategoryConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'updated') {
        this.getSkillCategoryList();
      }
    });
  }

  public editExistingSkill(element){

    let skillConstant = JSON.parse(JSON.stringify(LibraryConstant.CreateSkillDialogData));
    skillConstant.header='Edit Skill';
    skillConstant.formField[0].value = element['skillId'];
    skillConstant.formField[1].value = element['skillCategoryId'];
    skillConstant.formField[2].value = element['name'];
    skillConstant.formField[3].value = element['description'];
    skillConstant.btnlist[0].name = 'Edit';
    skillConstant.btnlist[0].callBackValue = CommonConstant.DialogModelApi.EditExistingSkill;

    let catOption = [];
    this.skillCategoryList.forEach(category => {
      const option = { key: category['name'], value: category['skillCategoryID'] };
      catOption.push(option);
    });
    skillConstant.formField[1].options = catOption;

    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: skillConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'updated') {
        this.getSkillList();
        this.skillSearchFormGroup.controls['catDropDown'].setValue('all');
      }
    });
  }

  public deleteSkillCategory(element){
    console.log(element);
    let skillCategoryConstant = JSON.parse(JSON.stringify(LibraryConstant.DeleteSkillConfirmation));
    skillCategoryConstant.btnMsg='Deleting skill category will also delete the skills listied with this category. Are you sure you want to delete?';
    
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '450px',
      data: skillCategoryConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
          this._skillService.DeleteSkillCategory(element['skillCategoryID']).subscribe(resp => {
          this.toastr.success(resp['ResponseInfo']);
          this.getSkillCategoryList();
        });
      }
    });
  }

  public deleteExistingSkill(element){
    let skillCategoryConstant = JSON.parse(JSON.stringify(LibraryConstant.DeleteSkillConfirmation));
    skillCategoryConstant.btnMsg='Are you sure you want to delete?';
    skillCategoryConstant.header='Delete Skill';
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '450px',
      data: skillCategoryConstant
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'confirm') {
        this._skillService.DeleteExistingSkill(element['skillId']).subscribe(resp => {
          this.toastr.success(resp['ResponseInfo']);
          this.getSkillList();
        });
      }
    });
  }
}
