import { CommonConstant } from 'src/app/common/common-constant';

export class LibraryConstant {
    public static CreateSkillCategoryDialogData = {
        header: 'Add Skill Category',
        msg: '',
        formField: [
            {
                label: 'Skill Category Id',
                fieldType: 'input',
                name: 'skillCategoryID',
                options: null,
                required: true,
                editable:false,
                value:null
            },
            {
                label: 'Skill Category',
                fieldType: 'input',
                name: 'name',
                options: null,
                required: true,
                editable:true,
                value:null
            }
        ],
        btnMsg: '',
        btnlist: [
            {
                name: 'Create',
                isPrimary: true,
                callBackValue: CommonConstant.DialogModelApi.CreateSkillCategory
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: CommonConstant.DialogModelApi.Cancel
            }
        ]
    }

    public static CreateSkillDialogData = {
        header: 'Add Skill',
        msg: '',
        formField: [
            {
                label: 'Skill Id',
                fieldType: 'input',
                name: 'skillId',
                options: null,
                required: true,
                editable:false,
                value:null
            },
            {
                label: 'Skill Category',
                fieldType: 'dropdown',
                name: 'catId',
                options: null,
                required: true,
                editable:true,
                value:null
            },
            {
                label: 'Skill Name',
                fieldType: 'input',
                name: 'name',
                options: null,
                required: true,
                editable:true,
                value:null
            },
            {
                label: 'Skill Description',
                fieldType: 'textarea',
                name: 'description',
                options: null,
                required: true,
                editable:true,
                value:null
            }
        ],
        btnMsg: '',
        btnlist: [
            {
                name: 'Create',
                isPrimary: true,
                callBackValue: CommonConstant.DialogModelApi.CreateNewSkill
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: CommonConstant.DialogModelApi.Cancel
            }
        ]
    }

    public static DeleteSkillConfirmation={
        header: 'Delete Skill Category',
        msg: '',
        formField:[],
        btnMsg: 'Are you sure you want to delete?',
        btnlist: [
            {
                name: 'Ok',
                isPrimary: true,
                callBackValue: CommonConstant.DialogModelApi.Confirm
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: CommonConstant.DialogModelApi.Cancel
            }
        ]
    }
}