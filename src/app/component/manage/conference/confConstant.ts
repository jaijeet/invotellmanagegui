export class ConfConstant{
    public static CreateConfDialogData = {
        header: 'Add Conference',
        msg: '',
        formField: [
            {
                label: 'Conference Name:',
                fieldType: 'input',
                name: 'ivrName',
                oprtions: null,
                required: true
            }
        ],
        btnMsg: "You will need to setup the Conference via the 'Settings' option.",
        btnlist: [
            {
                name: 'Save',
                isPrimary: true,
                callBackValue: null
            },
            {
                name: 'Cancel',
                isPrimary: false,
                callBackValue: null
            }
        ]
    }
}