import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import { DialogBoxComponent } from 'src/app/common/diaglog-box/dialog-box.component';
import { ConfConstant } from './confConstant';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('200ms ease-in', style({ transform: 'translateX(0%' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateX(100%' }))
      ])
    ])
  ]
})
export class ConferenceComponent implements OnInit {

  displayedColumns: string[] = ['createdOn', 'confName', 'setting', 'delete'];
  dataSource = new MatTableDataSource<ConfTableHeader>(CONF_LIST_DATA);
  showConfSettingPage:boolean=false;
  
  constructor(private dialog: MatDialog,private formBuilder:FormBuilder) { }

  ngOnInit() {
  }

  public createConferencePopUp() {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      disableClose: true,
      width: '550px',
      data: ConfConstant.CreateConfDialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  public confSetting(confId){
    console.log(confId);
    this.showConfSettingPage = true;
  }

  public deleteConfPopup(confId){

  }

  public backtoList() {
    this.showConfSettingPage = false;
  }
}

export interface ConfTableHeader {
  createdOn: number;
  confName: string;
  confId: number;
}

const CONF_LIST_DATA: ConfTableHeader[] = [
  { createdOn: 1567969066193, confName: 'Demo', confId: 101 },
  { createdOn: 1567759166193, confName: 'Test', confId: 102 },
  { createdOn: 1567259086193, confName: 'Humara', confId: 103 },
  { createdOn: 1567019066193, confName: 'Demo', confId: 101 },
  { createdOn: 1565009166193, confName: 'Test', confId: 102 },
  { createdOn: 1564509086193, confName: 'Humara', confId: 103 }
]