import { CommonConstant } from 'src/app/common/common-constant'

export class ReportConstant {
    public static AgentMonitoringTable=[
        {
            field:'caller',name:'Caller'
        },
        {
            field:'lastLogin',name:'Last Login'
        },
        {
            field:'onPhone',name:'On Phone'
        },
        {
            field:'callStart',name:'Call Start Time'
        },
        {
            field:'status',name:'Status'
        },
        {
            field:'talking',name:'Talking To'
        },
        {
            field:'direction',name:'Current Direction'
        },
        {
            field:'action',name:'Action'
        }
    ]
}