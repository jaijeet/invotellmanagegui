import { Component, OnInit, ViewChild } from '@angular/core';
import { ReportConstant } from './report.constant';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, transition, style, animate } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { tr } from 'date-fns/locale';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('200ms ease-in', style({ transform: 'translateX(0%' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateX(100%' }))
      ])
    ])
  ]
})
export class ReportComponent implements OnInit {

  viewMainReportPage:boolean=true;
  viewAgentMonitoringPage:boolean=false;
  viewQueueManagementPage:boolean=false;
  viewDownloadReportPage:boolean=false;
  showQueueSelection:boolean =false;
  showQueueDetailSection: boolean=false;

  ivrList=['demo','junior','admin'];
  queueList=['abc','iygi','jjhgiyuhao'];

  agentMonitorColumn=['caller','lastLogin','onPhone','callStart','status','talking','direction','action'];
  agentCols;

  headerName="";

  @ViewChild(MatSort,{static:false}) matSort: MatSort;
  agentMonitorDatasource:MatTableDataSource<any>;
  

  constructor() { }

  ngOnInit() {
    this.agentCols = ReportConstant.AgentMonitoringTable;
    this.agentMonitorDatasource = new MatTableDataSource(this.createDummyTable());
  }
  createDummyTable() {
    let tableData=[
      {
      'caller':'caller',
      'lastLogin':'last',
      'onPhone':'phone',
      'callStart':'callstart',
      'status':'status',
      'talking':'talking',
      'direction':'direct',
      'action':'show',
      'active':'listen'
    },
    {
      'caller':'caller1',
      'lastLogin':'last2',
      'onPhone':'phone1',
      'callStart':'callstart1',
      'status':'status1',
      'talking':'talking1',
      'direction':'direct1',
      'action':'exit',
      'active':'listen'
    },
    {
      'caller':'caller1',
      'lastLogin':'last2',
      'onPhone':'phone1',
      'callStart':'callstart1',
      'status':'status1',
      'talking':'talking1',
      'direction':'direct1',
      'action':'na',
      'active':'listen'
    }
  ];
  return tableData;
  }

  openMainReportPage(){
    this.viewMainReportPage=true;
    this.viewAgentMonitoringPage=false;
    this.viewQueueManagementPage=false;
    this.viewDownloadReportPage=false;
    this.showQueueSelection=false;
    this.showQueueDetailSection=false;
  }

  openAgentMonitorPage(){
    this.viewMainReportPage=false;
    this.viewAgentMonitoringPage=true;
    this.viewQueueManagementPage=false;
    this.viewDownloadReportPage=false;
    this.showQueueSelection=false;
    this.showQueueDetailSection=false;
  }

  openQueueManagementPage(){
    this.viewMainReportPage=false;
    this.viewAgentMonitoringPage=false;
    this.viewQueueManagementPage=true;
    this.viewDownloadReportPage=false;
    this.showQueueSelection=false;
    this.showQueueDetailSection=false;
  }

  openReportGenerationPage(type){
    this.viewMainReportPage=false;
    this.viewAgentMonitoringPage=false;
    this.viewQueueManagementPage=false;
    this.viewDownloadReportPage=true;
    this.showQueueSelection=false;
    this.showQueueDetailSection=false;
    if(type=="inbound"){
      this.headerName="Inbound Report";
    }else{
      this.headerName="Outbound Report";
    }
  }

  onIvrSelect(ivr){
    this.showQueueSelection=true;
    this.showQueueDetailSection=false
  }

  onSelectingQueue(queue){
    this.showQueueSelection=false;
    this.showQueueDetailSection=true;
  }
  generateReport(){

  }
}
