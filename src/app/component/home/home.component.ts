import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pageTitle: string;
  navList: any;
  constructor() { }

  ngOnInit() {
    this.pageTitle = "Dashboard";
    this.navList = [{
      'title': 'Dashboard',
      'route': '/home/dashboard'
    }];

  }

  pageUpdated(event) {
    this.pageTitle = event['title'];
    this.navList = event['nav'];
    console.log(event);
  }
}
