import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private domainPath = environment.DomainUrl;

  constructor(private http: HttpClient) { }

  public GetCpuUsageChart() {
    let url = this.domainPath + environment.ApiUrls.getCpuUsageChart;
    return this.http.get(url);
  }

  public GetCpuMemoryUsageChart() {
    let url = this.domainPath + environment.ApiUrls.getCpuMemoryUsageChart;
    return this.http.get(url);
  }
  
}
