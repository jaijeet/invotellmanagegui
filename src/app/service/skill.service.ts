import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkillService {
  private domainPath = environment.DomainUrl;

  constructor(private http: HttpClient) { }

  public GetSkillCategoryList(){
    let url = this.domainPath + environment.ApiUrls.getSkillCategoryList;
    return this.http.get(url);
  }

  public GetSkillList(){
    let url = this.domainPath + environment.ApiUrls.getSkillList;
    return this.http.get(url);
  }

  public AddSkillCategory(data){
    let url = this.domainPath + environment.ApiUrls.addSkillCategory;
    return this.http.post(url,data);
  }

  public AddNewSkill(data,catId){
    let url = this.domainPath + environment.ApiUrls.addNewSkill;
    url = url.replace("{catId}", catId);
    return this.http.post(url,data);
  }

  public EditSkillCategory(data,catId){
    let url = this.domainPath + environment.ApiUrls.editSkillCategory;
    url = url.replace("{catId}", catId);
    return this.http.put(url,data);
  }

  public EditExistingSkill(data,skillId,catId){
    let url = this.domainPath + environment.ApiUrls.editExistingSkill;
    url = url.replace("{skillId}", skillId);
    url = url.replace("{catId}", catId);
    return this.http.put(url,data);
  }

  public DeleteSkillCategory(catId:any=0,name:any=''){
    let url = this.domainPath+environment.ApiUrls.deleteSkillCategory;
    url = url.replace('{name}',name);
    url = url.replace('{catId}',catId);
    return this.http.delete(url);
  }

  public DeleteExistingSkill(skillId){
    let url = this.domainPath+environment.ApiUrls.deleteExistingSkill;
    url = url.replace('{skillId}',skillId);
    return this.http.delete(url);
  }
}