import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class IvrService {

  private domainPath = environment.DomainUrl;

  constructor(private http: HttpClient) { }

  public AddNewIvr(data: any) {
    let url = this.domainPath + environment.ApiUrls.addNewIvr;
    return this.http.post(url, data);
  }

  public GetExistingIvr(ivrId: any) {
    let url = this.domainPath + environment.ApiUrls.getExistingIvr;
    url = url.replace("{ivrId}", ivrId);
    return this.http.get(url);
  }

  public UpdateExistingIvr(ivrId: any, data: any) {
    let url = this.domainPath + environment.ApiUrls.updateExistingIvr;
    url = url.replace("{ivrId}", ivrId);
    return this.http.put(url, data);
  }

  public DeleteExistingIvr(ivrId: any) {
    let url = this.domainPath + environment.ApiUrls.deleteExistingIvr;
    url = url.replace("{ivrId}", ivrId);
    return this.http.delete(url);
  }

  public GetCompleteIvrList() {
    let url = this.domainPath + environment.ApiUrls.getCompleteIvrList;
    return this.http.get(url);
  }

  public GetIvrInfoByKeyword(ivrId: any, keyword: string) {
    let url = this.domainPath + environment.ApiUrls.getIvrInfoByKeyword;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{keyword}", keyword);
    return this.http.get(url);
  }

  public AddNewIvrInfo(ivrId: any, data: any) {
    let url = this.domainPath + environment.ApiUrls.addNewIvrInfo;
    url = url.replace("{ivrId}", ivrId);
    return this.http.post(url, data);
  }

  public UpdateExistingIvrInfo(ivrId: any, keyword: string, data: any) {
    let url = this.domainPath + environment.ApiUrls.updateExistingIvrinfo;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{keyword}", keyword);
    return this.http.put(url, data);
  }

  public DeleteExistingIvrInfo(ivrId: any, keyword: string, data: any) {
    let url = this.domainPath + environment.ApiUrls.deleteExistingIvrinfo;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{keyword}", keyword);
    return this.http.delete(url);
  }

  public UploadIvrFile(ivrId, keyword, data: File) {
    let formData: FormData = new FormData();
    formData.append('file', data, data.name);
    let headers = new HttpHeaders();
    const httpUploadOptions = {
      headers
    }
    let url = this.domainPath + environment.ApiUrls.ivrFileUpload;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{keyword}", keyword);

    return this.http.post(url, formData, httpUploadOptions);
  }

  public UpdateMultipleIvrInfo(ivrId,data){
    let url = this.domainPath + environment.ApiUrls.updateIvrInfoList;
    url = url.replace("{ivrId}", ivrId);
    return this.http.put(url, data);
  }

  public GetQueuePbxTreeByIvrId(ivrId){
    let url = this.domainPath + environment.ApiUrls.getQueuePbxTree;
    url = url.replace("{ivrId}", ivrId);
    return this.http.get(url);
  }
}
