import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QueueService {
  private domainPath = environment.DomainUrl;

  constructor(private http: HttpClient) { }

  public UploadQueuePromptFile(ivrId, qid, data: File) {
    let formData: FormData = new FormData();
    formData.append('file', data, data.name);
    let headers = new HttpHeaders();
    const httpUploadOptions = {
      headers
    }
    let url = this.domainPath + environment.ApiUrls.queuePromptFileUpload;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{queueId}", qid);

    return this.http.post(url, formData, httpUploadOptions);
  }


  public AddNewVoicePrompt(ivrId,qid,parentNode,status,data,file){
    let url = this.domainPath+environment.ApiUrls.addVoicePrompt;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{queueId}", qid);
    url = url.replace("{parentNode}", parentNode);    
    url = url.replace("{status}", status);
    url = url.replace("{file}",file);
    return this.http.post(url,data);
  }

  public AddNewQueue(ivrId,parentNode,data){
    let url = this.domainPath+environment.ApiUrls.addNewQueue;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{parentNode}", parentNode);
    return this.http.post(url,data);
  }

  public AddQueueInfoList(ivrId,qid,data){
    let url = this.domainPath+environment.ApiUrls.addQueueInfoList;
    url = url.replace("{ivrId}", ivrId);
    url = url.replace("{queueId}", qid);
    return this.http.post(url,data); 
  }
}
