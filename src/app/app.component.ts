import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'manage';
  constructor( private matIconRegistry: MatIconRegistry,  private domSanitizer: DomSanitizer){
    this.matIconRegistry.addSvgIcon("agent_monitor", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/monitor.svg"));
    this.matIconRegistry.addSvgIcon("queue_manage", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/management.svg"));
    this.matIconRegistry.addSvgIcon("inbound_report", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/inbound.svg"));
    this.matIconRegistry.addSvgIcon("outbound_report", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/outbound.svg"));
    this.matIconRegistry.addSvgIcon("enable_monitor", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/enable_monitor.svg"));
    this.matIconRegistry.addSvgIcon("listen", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/listen.svg"));
    this.matIconRegistry.addSvgIcon("bargein", this.domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/barge.svg"));
    
  }
}
